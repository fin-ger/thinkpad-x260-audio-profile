# Lenovo Thinkpad X260 Audio Profile

## Usage with Linux (Pipewire)

Import the APO profile into
[Easy Effects](https://flathub.org/apps/com.github.wwmm.easyeffects)
by adding an Equalizer effect and then importing the APO profile from this
repository.

Then set the output volume to **-5.0 dB**.

## Notes

This audio profile is meant to be used for the laptop sitting on a desk with a
solid surface. As the X260 has downfiring speakers, different profiles for a
laptop seated on a desk and for sitting on your lap are required. This
profile is for desk usage and will sound utterly garbage on your lap.

## Issues

Currently the mid-bass is still quite mushed and the higher mid-tones have bad
stereo separation. Maybe I'll work on it on another day. Overall I would say
it's a 60% improvement over stock audio quality. Still a lot to gain.
